const canv = document.getElementById('canvas'),
      ctx = canv.getContext('2d');
canv.width = window.innerWidth;
canv.height = window.innerHeight-150;
let isMouseDown = false,
    cordsCanv = []
    objFormPaint = null,
    lineColor = "#000",
    widthLine = 5
    timeClient = 30;

formPaint.addEventListener('submit', (e)=>{
  e.preventDefault();
  objFormPaint = Object.fromEntries(new FormData(e.target).entries());
  widthLine = objFormPaint.widthLine;
  timeClient = objFormPaint.timer;
});

color.addEventListener('change', e =>{
  lineColor = e.target.value;
  ctx.strokeStyle = lineColor;
  ctx.fillStyle = lineColor;
})

canv.addEventListener('mousedown',()=> isMouseDown = true);
canv.addEventListener('mouseup',()=> {
  isMouseDown = false;
  ctx.beginPath();
  cordsCanv.push('mousedown');
});



canv.addEventListener('mousemove', e =>{
  if(isMouseDown){
    paint(e);
    cordsCanv.push([e.clientX,e.clientY, lineColor]);
  }
});
/* Function Seve, Replay, Clear */
const
  paint = e => {
    ctx.strokeStyle = e.lineColor;
    ctx.fillStyle = e.lineColor;
    ctx.lineWidth = widthLine * 2;

    ctx.lineTo(e.clientX, e.clientY);
    ctx.stroke();

    ctx.beginPath();
    ctx.arc(e.clientX, e.clientY, widthLine, 0, Math.PI *2);
    ctx.fill();

    ctx.beginPath();
    ctx.moveTo(e.clientX, e.clientY);
  }
    replay = time => {
        let timer = setInterval(()=>{
            if(!cordsCanv.length){
              clearInterval(timer);
              ctx.beginPath();
              return;
            }
            let crd = cordsCanv.shift(),
              e = {
                clientX: crd['0'],
                clientY: crd['1'],
                lineColor: crd['2']
              };
            paint(e);
        },time);

    },
    seve = () => {
      localStorage.setItem('cordsCanv', JSON.stringify(cordsCanv));
    },
    clear = () => {
      ctx.fillStyle = 'rgb(255,255,255)';
      ctx.fillRect(0,0,canv.width,canv.height);
      ctx.beginPath();
      ctx.fillStyle = 'rgb(0,0,0)';
  };

document.addEventListener('keydown',(e) => {
  if (e.keyCode == 83) {
    // save
    seve();
    console.log('seve...');
  }
  if (e.keyCode == 82) {
    //replay
    cordsCanv =localStorage.getItem('cordsCanv') ? JSON.parse(localStorage.getItem('cordsCanv')) : [];
    replay(timeClient);
    clear();
    console.log('replay...' , timeClient);
  }
  if (e.keyCode == 67) {
    //clear
    clear();
    console.log('clear');
  }
  if (e.keyCode == 81) {
    localStorage.removeItem('cordsCanv');
    cordsCanv = [];
    clear();
  }
});



